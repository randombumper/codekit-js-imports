// Importamos las dependencias de nuestro módulo.
// Cuando las dependencias no exportan nada (simplemente se ejecuta su Javascript),
// lo hacemos directamente con import 'modulo'
import 'jquery'
import 'slick-carousel'

// Cuando las dependencias exportan objetos (librerías, ...), lo hacemos de esta
// otra manera
import md5 from 'md5'

// Inicializamos el carrusel
$("[data-slick]").slick()

// Ejemplo de uso del objeto importado md5
$("#hash").text(md5('¡Hola mundo!'))