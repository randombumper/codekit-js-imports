'use strict';

System.register(['jquery', 'slick-carousel', 'md5'], function (_export, _context) {
  "use strict";

  var md5;
  return {
    setters: [function (_jquery) {}, function (_slickCarousel) {}, function (_md) {
      md5 = _md.default;
    }],
    execute: function () {

      // Inicializamos el carrusel
      $("[data-slick]").slick();

      // Ejemplo de uso del objeto importado md5


      // Cuando las dependencias exportan objetos (librerías, ...), lo hacemos de esta
      // otra manera
      // Importamos las dependencias de nuestro módulo.
      // Cuando las dependencias no exportan nada (simplemente se ejecuta su Javascript),
      // lo hacemos directamente con import 'modulo'
      $("#hash").text(md5('¡Hola mundo!'));
    }
  };
});
